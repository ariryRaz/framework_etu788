/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author tsikyrami
 */

import java.lang.reflect.*;
import java.sql.Connection;
import java.util.List;
import java.util.Map;
import model.HttpMethod;
import model.ModelView;
import model.Transaction;
import model.Url;
import util.DbGeneric;
import util.ReflectControlleur;
import static util.ReflectControlleur.getClassAttributes;

public class Main {
    public static void main(String[] args) throws Exception {
        
        System.out.println("ETU000771 Ramitandrintsoa tsiky");
       
        Connection con = new DbGeneric("postgres").connect();
        
        //Test Ajout Base En appelant un lien qui va appeler un controlleur    
        
        /*
             //Test url_1 =  http://localhost:8080/moneako_etu771/TransactionControlller/add.gen
            Url u2 =new Url("/moneako_etu771/TransactionController/add.gen",HttpMethod.POST);
            System.out.println("Url taper = "+u2.getUrl());
            System.out.println("Controlleur = "+u2.getControlleur());
            System.out.println("Method = "+u2.getMethod());
            System.out.println("TypeMethod = "+u2.getTypeMethod());

            ModelView modelview2 = ReflectControlleur.getResult(u2,new Transaction("TR000001","Test1000","15430",500000,"Achat"),con);
            Map<String, Object> data2 = modelview2.getData();
            for (Map.Entry<String, Object> entry : data2.entrySet()) {
                String key = entry.getKey();
                Transaction value =  (Transaction) entry.getValue();
                System.out.println("Clé : " + key + ", Id : " + value.getId()+ ", Nom : " + value.getNom()+ ", Matricule : " + value.getMatricule()+ ", Valeur : " + value.getValeur()+ ", Type : " + value.getType()  );
            }
        
          */  
           
            
        //Test Update Base En appelant un lien qui va appeler un controlleur    
        /*
            //Test url_2=  http://localhost:8080/moneako_etu771/TransactionControlller/update_base.gen
            Url u2 =new Url("/moneako_etu771/TransactionController/update.gen",HttpMethod.POST);
            System.out.println("Url taper = "+u2.getUrl());
            System.out.println("Controlleur = "+u2.getControlleur());
            System.out.println("Method = "+u2.getMethod());
            System.out.println("TypeMethod = "+u2.getTypeMethod());

            ModelView modelview2 = ReflectControlleur.getResult(u2,new Transaction("TR10","Test1","67890",111111,"Achat"),con);
            Map<String, Object> data2 = modelview2.getData();
            for (Map.Entry<String, Object> entry : data2.entrySet()) {
                String key = entry.getKey();
                Transaction value =  (Transaction) entry.getValue();
                System.out.println("Clé : " + key + ", Id : " + value.getId()+ ", Nom : " + value.getNom()+ ", Matricule : " + value.getMatricule()+ ", Valeur : " + value.getValeur()+ ", Type : " + value.getType()  );
            }
            
        */
        
        /*
            
         //Test List Base En appelant un lien qui va appeler un controlleur    
            
            //Test url_3 =  http://localhost:8080/moneako_etu771/TransactionControlller/list_base.gen?id=TR_B1
            Url u3 =new Url("/moneako_etu771/TransactionController/list.gen?id=TR_B1",HttpMethod.GET);
            System.out.println("Url taper = "+u3.getUrl());
            System.out.println("Controlleur = "+u3.getControlleur());
            System.out.println("Method = "+u3.getMethod());
            System.out.println("TypeMethod = "+u3.getTypeMethod());
            
            // Avec Recherche 
            Object request = new Transaction("TR_B1",null,null,null,null);
        
            //Model view
            ModelView modelview3 = ReflectControlleur.getResult(u3,request,con);
            Map<String, Object> data3 = modelview3.getData();
            
            //page de retour
            System.out.println("Page retourner = "+modelview3.getUrl());
               
            //boucle resultat
            for (Map.Entry<String, Object> entry : data3.entrySet()) {
                String key = entry.getKey();
                Transaction value =  (Transaction) entry.getValue();
                //  System.out.println(value.getId());
                 System.out.println("Clé : " + key + ", Id : " + value.getId()+ ", Nom : " + value.getNom()+ ", Matricule : " + value.getMatricule()+ ", Valeur : " + value.getValeur()+ ", Type : " + value.getType()  );
            }

         */
        
        
                 //Test List efa misy ao am controlleur fa tsy maka am base 
            
            //Test url_3 =  http://localhost:8080/moneako_etu771/TransactionControlller/list_test.gen
            Url u3 =new Url("/moneako_etu771/TransactionController/list_test.gen",HttpMethod.GET);
            System.out.println("Url taper = "+u3.getUrl());
            System.out.println("Controlleur = "+u3.getControlleur());
            System.out.println("Method = "+u3.getMethod());
            System.out.println("TypeMethod = "+u3.getTypeMethod());
            
        
            //Model view
            ModelView modelview3 = ReflectControlleur.getResult(u3,null,con);
            Map<String, Object> data3 = modelview3.getData();
            
            //page de retour
            System.out.println("Page retourner = "+modelview3.getUrl());
               
            //boucle resultat
            for (Map.Entry<String, Object> entry : data3.entrySet()) {
                String key = entry.getKey();
                Transaction value =  (Transaction) entry.getValue();
                //  System.out.println(value.getId());
                 System.out.println("Clé : " + key + ", Id : " + value.getId()+ ", Nom : " + value.getNom()+ ", Matricule : " + value.getMatricule()+ ", Valeur : " + value.getValeur()+ ", Type : " + value.getType()  );
            }
        
        
            
    }
}



