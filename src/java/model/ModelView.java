/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author tsikyrami
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModelView<T> {
    private String url;
    private Map<String, Object> data;

    public String getUrl() {
            return url;
    }
    public Map<String, Object> getData() {
            return data;
    }
    public void setUrl(String url) {
            this.url = url;
    }
    public void setData(Map<String, Object> data) {
            this.data = data;
    }

    public ModelView(String url) {
            super();
            this.setUrl(url);
            this.data = new HashMap<String, Object>();
    }
    public ModelView(String url, Map<String, Object> data) {
            super();
            this.setUrl(url);
            this.setData(data);
    }
    
    public <T> List<T> CastTo(String className) throws ClassNotFoundException{
        Class<?> clazz = Class.forName("model."+className);
        List<T> val = new ArrayList<>();
        for(Map.Entry<String, Object> data : this.getData().entrySet()) {
             T castObj = (T) clazz.cast(data.getValue());
             val.add(castObj);
        }
        return val;
    }
    
    
    public void  add (String key,Object obj){
        this.getData().put(key,obj);
    }
}
