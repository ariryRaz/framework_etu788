/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author tsikyrami
 */
public class Url {
    String url;
    HttpMethod typemethod;
    String controlleur;
    String method;
    
    public static final String split_Project ="/";
    public static final String project="framework_etu788";
    public static final String split_Controlleur="/";
    public static final String split_Method="/";
    public static final String fin_url=".gen";
    
    public Url(){}
    
    public Url(String url){
        this.removeQueryString(url);
        this.setUrl(url);
    }
    
    public String removeQueryString(String url) {
        if (url == null) return null;
        int index = url.indexOf('?');
        if (index == -1) return url;
        return url.substring(0, index);
    }
    
    public Url(String url,HttpMethod typemethod){
        this.setUrl(url);
        this.setTypeMethod(typemethod);
        this.setControlleur();
        this.setMethod();
    }
    
   public HttpMethod getTypeMethod(){
        return this.typemethod;
    } 
    
    
    public void setTypeMethod(HttpMethod typemethod){
        this.typemethod=typemethod;
    }
    
    
    public String getNameProject(){
        return project;
    }
    
    public String getUrl(){
        return this.url;
    }
    
    public void setUrl(String url){
        this.url = url;
    }
    
    public void setControlleur(){
        String url = this.getUrl();
        url = removeQueryString(url);
        url =  url.replace(split_Project.concat(project).concat(split_Controlleur),"");
        url =  url.replace(fin_url,"");
        String[] segments = url.split(split_Method);
        this.controlleur= segments[0];
    }
    
    public void setControlleur(String url){
        url = removeQueryString(url);
        url =  url.replace(split_Project.concat(project).concat(split_Controlleur),"");
        url =  url.replace(fin_url,"");
        String[] segments = url.split(split_Method);
        this.controlleur= segments[0];
    }
    
    public String getControlleur(){
        return this.controlleur;
    }
    
    public void setMethod(){
        String url = this.getUrl();
        url = removeQueryString(url);
        url =  url.replace(split_Project.concat(project).concat(split_Controlleur).concat(this.getControlleur()).concat(this.split_Method),"");
        url =  url.replace(fin_url,"");
        this.method=url;
    }
       
    public void setMethod(String url){
        url = removeQueryString(url);
        url =  url.replace(split_Project.concat(project).concat(split_Controlleur).concat(this.getControlleur()).concat(this.split_Method),"");
        url =  url.replace(fin_url,"");
        this.method=url;
    }
    
    public String getMethod(){
        return this.method;
    }
    
    @Override
    public String toString() {
        String str = "{url: " + this.url + "method: " + this.getMethod()+"}";
        return str;
    }
    
    
}
