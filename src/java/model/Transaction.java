/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author tsikyrami
 */
public class Transaction {
    String id;
    String nom;
    String matricule;
    Double valeur;
    String type;

    
    public Transaction(){
        
    }
    
    public Transaction(String id, String nom, String matricule, Double valeur, String type) {
        this.id = id;
        this.nom = nom;
        this.matricule = matricule;
        this.valeur = valeur;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public Double getValeur() {
        return valeur;
    }

    public void setValeur(Double valeur) {
        this.valeur = valeur;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
