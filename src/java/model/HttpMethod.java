/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author tsikyrami
 */
public enum HttpMethod {
	GET,
	POST;
	
	public static HttpMethod parse(String methodString) {
		methodString = methodString.toLowerCase();
		switch (methodString) {
                    case "get":
                            return HttpMethod.GET;
                    case "post":
                            return HttpMethod.POST;
                    default:
                            return HttpMethod.GET;
		}
	}
}

