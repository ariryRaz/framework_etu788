/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.sql.SQLException;
import model.ModelView;

/**
 *
 * @author tsikyrami
 */
public interface InterfaceController {
    public ModelView list(Connection con,Object tr) throws SQLException, InstantiationException, IllegalAccessException;
    public ModelView add(Connection con,Object tr) throws SQLException, InstantiationException, IllegalAccessException;
    public ModelView update(Connection con, Object tr) throws SQLException, InstantiationException, IllegalAccessException;
    public ModelView delete(Connection con, Object tr)throws SQLException, InstantiationException, IllegalAccessException;
}
