/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import annotation.Controller;
import annotation.MyRequestMapping;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.HttpMethod;
import model.ModelView;
import model.Transaction;
import model.HttpMethod;
import util.DbGeneric;
import util.Generic;

/**
 *
 * @author tsikyrami
 */
@Controller("TransactionController")
public class TransactionController implements InterfaceController{

    public String pagedefault = "transaction.jsp";
    public String classdefault = "Transaction";
    
    public TransactionController() {}
  
    public TransactionController(String pagedefault,String classdefault){
        this.setClassdefault(classdefault);
        this.setPagedefault(pagedefault);
    }
    
    public String getPagedefault() {
        return pagedefault;
    }

    public void setPagedefault(String pagedefault) {
        this.pagedefault = pagedefault;
    }

    public String getClassdefault() {
        return classdefault;
    }

    public void setClassdefault(String classdefault) {
        this.classdefault = classdefault;
    }
    
    @MyRequestMapping(value = "list_test", method = HttpMethod.GET)
    public ModelView list_test(Connection con,Object o) throws SQLException {
        ModelView render = null;
        try{
            render = new ModelView(this.pagedefault);
            render.add("TRN1", new Transaction("TR1","Alicia","1543",500000.0,"Achat"));
            render.add("TRN2", new Transaction("TR2","Jean","1548",550000.0,"Vente"));
        }catch(Exception ex){
           Logger.getLogger(TransactionController.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            if(con != null) con.close();
        }
        return render;
    }
    
    @MyRequestMapping(value = "transactionadd", method = HttpMethod.GET)
    public ModelView transactionadd(Connection con,Object o) throws SQLException {
        ModelView render = null;
        try{
           render = new ModelView("transactionadd.jsp");   
        }catch(Exception ex){
           Logger.getLogger(TransactionController.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            if(con != null) con.close();
        }
        return render;
    }
    
    @MyRequestMapping(value = "transactionup", method = HttpMethod.GET)
    public ModelView transactionup(Connection con,Object o) throws SQLException {
        ModelView render = new ModelView("transactionup.jsp");
        try{
            List<Transaction> tr =  new Generic().select(Transaction.class,this.getClassdefault()," id = '"+((Transaction) o).getId()+"'",con); 
            for(Transaction t : tr)  render.add("KEY_"+t.getId(),t);
        }catch(Exception ex){
           Logger.getLogger(TransactionController.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            if(con != null) con.close();
        }
        return render;
    }
    
    @MyRequestMapping(value = "chart", method = HttpMethod.GET)
    public ModelView chart(Connection con,Object o) throws SQLException, InstantiationException, IllegalAccessException {
        ModelView render = new ModelView("chart.jsp");
        try {
            List<Transaction> tr =  new Generic().select(Transaction.class,this.getClassdefault(),null,con);
            for(Transaction t : tr) render.add("KEY_"+t.getId(),t);
        }catch(Exception ex){
           Logger.getLogger(TransactionController.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            if(con != null) con.close();
        }
        return render;
    }
    
    @Override
    @MyRequestMapping(value = "list", method = HttpMethod.GET)
    public ModelView list(Connection con,Object obj) throws SQLException, InstantiationException, IllegalAccessException {
        ModelView render = new ModelView(this.pagedefault);
        Transaction trans = null ;
        String condition ="";

        try{
            if(obj != null){
                trans = (Transaction) obj;
                condition  = new Generic().generateWhereClause(trans,this.getClassdefault());
            }
            List<Transaction> tr =  new Generic().select(Transaction.class,this.getClassdefault(),condition,con);
            for(Transaction t : tr) render.add("KEY_"+t.getId(),t);
        }catch(Exception ex){
           Logger.getLogger(TransactionController.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            if(con != null) con.close();
        }
        return render;
    }
    
    @Override    
    @MyRequestMapping(value = "add", method = HttpMethod.POST)
    public ModelView add(Connection con,Object tr) throws SQLException, InstantiationException, IllegalAccessException {
        ModelView render = new ModelView(this.pagedefault);
        try {
            new Generic().insert(tr,con);
            List<Transaction> tr2 =  new Generic().select(Transaction.class,this.getClassdefault(),null,con);
            for(Transaction t : tr2) render.add("KEY_"+t.getId(),t);
        } catch (SQLException ex) {
            Logger.getLogger(TransactionController.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            if(con != null) con.close();
        }
        return render;
    }
    
    @Override
    @MyRequestMapping(value = "update", method = HttpMethod.POST)
    public ModelView update(Connection con, Object tr) throws SQLException, InstantiationException, IllegalAccessException {
        Transaction trans = (Transaction) tr;
        ModelView render = new ModelView(this.pagedefault);
        try {
            new Generic().update(tr,con);
            List<Transaction> tr2 =  new Generic().select(Transaction.class,this.getClassdefault(),null,con);
            for(Transaction t : tr2) render.add("KEY_"+t.getId(),t);
        } catch (SQLException ex) {
            Logger.getLogger(TransactionController.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            if(con != null) con.close();
        }
        return render;
    }
    
    @Override    
    @MyRequestMapping(value = "delete", method = HttpMethod.GET)
    public ModelView delete(Connection con, Object obj) throws SQLException, InstantiationException, IllegalAccessException {
        ModelView render = new ModelView(this.pagedefault);
        try {
            new Generic().delete("transaction",((Transaction) obj).getId(),con);
            List<Transaction> tr2 =  new Generic().select(Transaction.class,this.getClassdefault(),null,con);
            for(Transaction t : tr2) render.add("KEY_"+t.getId(),t);
        } catch (SQLException ex) {
            Logger.getLogger(TransactionController.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            if(con != null) con.close();
        }
        return render;
    }

}
