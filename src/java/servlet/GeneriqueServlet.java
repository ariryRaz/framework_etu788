/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.HttpMethod;
import model.ModelView;
import model.Url;
import util.ReflectControlleur;
import static util.ReflectControlleur.getClassAttributes;
import static util.ReflectControlleur.createObjectFromMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author tsikyrami
 */
public class GeneriqueServlet extends HttpServlet {
    final String pageDefault = "../view/index.jsp";
    

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
   
        Object obj = this.GetObj(request);
        Url u =new Url(request.getRequestURI(),HttpMethod.parse(request.getMethod()));
        ModelView modelview = ReflectControlleur.getResult(u,obj,null);
        request.setAttribute("pageName", modelview.getUrl());
        request.setAttribute("data", modelview);
        request.getRequestDispatcher(this.pageDefault).forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(GeneriqueServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(GeneriqueServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    protected  Object GetObj(HttpServletRequest request) throws Exception{
        Object obj = null;
        Map<String, String> attributes = new HashMap<>();
        String classe="";
        String type_obj ="";
        String id_value ="";
        if((String) request.getParameter("classe")!=null) classe = (String) request.getParameter("classe");
        if(((String) request.getParameter("obj"))!=null) type_obj = (String) request.getParameter("obj");
        if(((String) request.getParameter("id"))!=null) id_value = (String) request.getParameter("id");
        
        if(type_obj.equals("insert") || type_obj.equals("update") ||  type_obj.equals("select") ){
            attributes = getClassAttributes(classe);    
            for (Map.Entry<String, String> entry : attributes.entrySet()) {
                String val = (String) request.getParameter(entry.getKey());
                if(val.equals("")) val = null;
                if((String) request.getParameter(entry.getKey())!=null) entry.setValue(val);
                else entry.setValue(null);
            }
            obj = createObjectFromMap(classe,attributes);
        }else if(type_obj.equals("passe_id") || type_obj.equals("delete")){
            attributes.put("id", id_value);
            obj = createObjectFromMap(classe,attributes);
        }
        return obj;
    }

}
