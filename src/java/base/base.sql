/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  tsiky
 * Created: Jun 13, 2023
 */



--bdd
create database moneako;


--table
CREATE TABLE transaction (
	id varchar(255) NOT NULL,
	nom varchar(255) NULL,
	matricule varchar(255) NULL,
	valeur float8 NULL,
	"type" varchar(255) NULL,
	CONSTRAINT transaction_pkey PRIMARY KEY (id)
);

--data
INSERT INTO transaction
(id, nom, matricule, valeur, "type") 
VALUES('TR_1682457673132', 'Savon', '001', 1000.0, 'Achat');
INSERT INTO transaction
(id, nom, matricule, valeur, "type")
VALUES('TR_1682457740886', 'Cahier', '002', 2000.0, 'Achat');
