/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import annotation.MyRequestMapping;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.HttpMethod;
import model.ModelView;
import model.Url;

/**
 *
 * @author tsikyrami
 */
public  class ReflectControlleur {
    
    public ReflectControlleur(){
        
    }

    public static ModelView getResult(Url u,Object o,Connection con) throws Exception {
        ModelView modelView = null;
        Class<?> controllerClass;

        String controllerClassName = "controller.".concat(u.getControlleur());
     
        controllerClass = Class.forName(controllerClassName);
        Method[] methods = controllerClass.getMethods();
        for (Method method : methods) {
            
            MyRequestMapping annotation = method.getAnnotation(MyRequestMapping.class);
            
            if (annotation != null && annotation.value().equals(u.getMethod()) && annotation.method().equals(u.getTypeMethod())) {
                modelView =(ModelView) method.invoke(controllerClass.newInstance(),con,o);
                System.out.println(modelView.getUrl());
                return modelView;
            }
        }
        throw new Exception("Méthode non trouvée");
    }
    
    
    public static Map<String, String> getClassAttributes(String className) {
        Map<String, String> attributes = new HashMap<>();
        try {
            Class<?> clazz = Class.forName("model.".concat(className));
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                String attributeName = field.getName();
                String attributeType = field.getType().getSimpleName();
                attributes.put(attributeName, attributeType);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return attributes;
    }
    
    
    private static Object convertStringToType(String value, Class<?> type) {
        Object obj = null;
        System.out.println("Valeur anle value=>"+value);
           System.out.println("Valeur anle type=>"+type);
                   
           
        if(value == null){
            return obj;
        }else{
            if (type == String.class) {
                obj = value;
            } else if (type == int.class || type == Integer.class) {
                obj =  Integer.parseInt(value);
            } else if (type == double.class || type == Double.class) {
                obj =  Double.parseDouble(value);
            } else if (type == boolean.class || type == Boolean.class) {
                obj =  Boolean.parseBoolean(value);
            } 
        }
          return obj; 
    }
    
     public static Object createObjectFromMap(String classe, Map<String, String> map) throws Exception {
         Class clazz = Class.forName("model.".concat(classe));
        Constructor<?> constructor = clazz.getConstructor();
        Object object = constructor.newInstance();
        for (Map.Entry<String, String> entry : map.entrySet()) {
  
            String attributeName = entry.getKey();
            String attributeValue = entry.getValue();
            Field field = clazz.getDeclaredField(attributeName);
            field.setAccessible(true);
            Class<?> fieldType = field.getType();
            System.out.println("attributeName=>"+attributeName);
            System.out.println("attributeValue=>"+attributeValue);
            System.out.println("fieldType=>"+fieldType);
            
            Object convertedValue = convertStringToType(attributeValue, fieldType);
            field.set(object, convertedValue);
        }
        return object;
    }
    
    
    /*
    public static ModelView GetResult(String nomcontrolleur,String method){
        ModelView modelView = null;
        Class<?> controllerClass;
            try{
                // Nom de la classe du contrôleur
                String controllerClassName = "controller.".concat(nomcontrolleur);

                // Charger la classe du contrôleur
                controllerClass = Class.forName(controllerClassName);
                
                // Récupérer l'instance du contrôleur
                Object controllerInstance = controllerClass.newInstance();

                // Appeler la méthode getList() du contrôleur
                Method getListMethod = controllerClass.getMethod(method);
                modelView  = (ModelView) getListMethod.invoke(controllerInstance); 
            }catch(Exception e){
                e.getMessage();
            }finally{
                 return modelView;
            }
    }
    */
}
