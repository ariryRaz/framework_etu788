/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author tsikyrami
 */
public class DbGeneric {
    public Connection con;
    public String Type_base = "postgres";
    
    public DbGeneric(String type_base){
        this.setType_base(type_base);
    }
    
    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public String getType_base() {
        return Type_base;
    }

    public void setType_base(String Type_base) {
        this.Type_base = Type_base;
    }

    public Connection connect() throws SQLException{
        try{
            if(this.getType_base()!=null){
                if(this.getType_base().equals("postgres")) this.setCon(new DbPostgres().connect());
                else if (this.getType_base().equals("mysql")) this.setCon(new DbMysql().connect());
            }else new Exception("Nom de base null");
        }catch(Exception e){
            System.out.println("Nom du Bdd='"+this.getType_base() +"' Erreur au niveau du connection au BDD");
            e.getMessage();
        }
        
       
        return this.getCon();
    }
    
}
