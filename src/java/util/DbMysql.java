/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tsikyrami
 */
public class DbMysql implements DbInterface{
    private Connection connection;

    public DbMysql(){
        
    }
    
    public DbMysql(String url, String user, String password) {
        try {
            this.connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public Connection connect() throws SQLException {
                if (this.connection == null || this.connection.isClosed()) {
            try {
                Class.forName("org.postgresql.Driver");
                this.connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/moneako", "mysql", "root");
                this.connection.setAutoCommit(false);
                return this.getCon();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(DbPostgres.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return this.getCon();
    }
    

    @Override
    public <T> ResultSet executeQuery(PreparedStatement statement) throws SQLException {
        return statement.executeQuery();
    }

    @Override
    public int executeUpdate(PreparedStatement statement) throws SQLException {
        return statement.executeUpdate();
    }

    @Override
    public void close() throws SQLException {
        if (this.connection != null && !this.connection.isClosed()) {
            this.connection.close();
        }
    }

    @Override
    public Connection getCon() throws SQLException {
        return this.connection;
    }
    
}
