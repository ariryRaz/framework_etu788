/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static javafx.scene.input.KeyCode.T;
import model.Transaction;
import model.ModelView;

/**
 *
 * @author tsikyrami
 */
public class Generic<T>{ 
    
    
    public List<T> select(Class<T> cl, String table, String condition, Connection conn) throws SQLException, InstantiationException, IllegalAccessException {
        int con =0;
        List<T> result = new ArrayList<>();
        String selectSql = "SELECT * FROM " + table;
        try{
            if(conn==null){
                con=1;               
               //Postgres
               conn =  new DbPostgres().connect();
            }
            if (condition != null && !condition.isEmpty()) selectSql += " WHERE " + condition;
            System.out.println("requete select ="+selectSql);
            PreparedStatement statement = conn.prepareStatement(selectSql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                T obj = cl.newInstance();
                for (Field field : cl.getDeclaredFields()) {
                    String fieldName = field.getName();
                    field.setAccessible(true);
                    Object value = resultSet.getObject(fieldName);
                    field.set(obj, value);
                }
                result.add(obj);
            }
        }catch(Exception e){
            conn.rollback();
            e.printStackTrace();
        }finally{
            conn.commit();
            if(con==1 && conn != null) conn.close();
        }
        return result;
    }
    
    /*
        public List<T> select(Class<T> cl, String table, String colonne , String condition, Connection conn) throws SQLException, InstantiationException, IllegalAccessException {
            List<T> result = new ArrayList<>();
            String selectSql = "SELECT " +colonne+" FROM " + table;
            if (condition != null && !condition.isEmpty()) {
                selectSql += " WHERE " + condition;
            }
            PreparedStatement statement = conn.prepareStatement(selectSql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                T obj = cl.newInstance();
                for (Field field : cl.getDeclaredFields()) {
                    String fieldName = field.getName();
                    field.setAccessible(true);
                    Object value = resultSet.getObject(fieldName);
                    field.set(obj, value);
                }
                result.add(obj);
            }
            return result;
        }
*/
    
    
     public static <T> void insert(T obj, Connection conn) throws SQLException {
        int con =0; 
        String tableName = obj.getClass().getSimpleName().toLowerCase();
        Field[] fields = obj.getClass().getDeclaredFields();
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        
        try{
            if(conn==null){
                con=1;               
               //Postgres
               conn =  new DbPostgres().connect();
            }
            for (Field field : fields) {
                String name = field.getName();
                sb.append(name).append(",");
                sb2.append("?").append(",");
            }
            
            sb.deleteCharAt(sb.length() - 1);
            sb2.deleteCharAt(sb2.length() - 1);
            String sql = "INSERT INTO " + tableName + " (" + sb.toString() + ") VALUES (" + sb2.toString() + ")";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            int i = 1;
            
            for (Field field : fields) {
                field.setAccessible(true);
                try {
                    pstmt.setObject(i, field.get(obj));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                i++;
            }
            
            pstmt.executeUpdate();
            
        }catch(Exception e){
            conn.rollback();
            e.printStackTrace();
        }finally{
            conn.commit();
            if(con==1 && conn != null) conn.close();
        }
    }
     
     
  public void update(T obj, Connection conn) throws SQLException, IllegalAccessException {
        Class<?> clazz = obj.getClass();
        String idColumn = "id"; //Atw Id le class
        Field idField = null;
        int con =0;
        try {
            if(conn==null){
                con=1;               
               //Postgres
               conn =  new DbPostgres().connect();
            }
            
            idField = clazz.getDeclaredField(idColumn);
            idField.setAccessible(true);
            Object idValue = idField.get(obj);
            
             String updateSql = "UPDATE " + clazz.getSimpleName() + " SET ";
            int index = 1;
            for (Field field : clazz.getDeclaredFields()) {
                String fieldName = field.getName();
                if (fieldName.equals(idColumn)) {
                    continue; // Ignore the ID column
                }
                field.setAccessible(true);
                Object newValue = field.get(obj);
                updateSql += fieldName + "=?";
                if (index < clazz.getDeclaredFields().length - 1) {
                    updateSql += ", ";
                }
                index++;
            }

            updateSql += " WHERE " + idColumn + "=?";
            PreparedStatement statement = conn.prepareStatement(updateSql);
            index = 1;
            for (Field field : clazz.getDeclaredFields()) {
                String fieldName = field.getName();
                if (fieldName.equals(idColumn)) {
                    continue; // Ignore the ID column
                }
                field.setAccessible(true);
                Object newValue = field.get(obj);
                statement.setObject(index, newValue);
                index++;
            }
            idField.setAccessible(true);
            idValue = idField.get(obj);
            statement.setObject(index, idValue);
            statement.executeUpdate();
            
            }catch(Exception e){
                conn.rollback();
                e.printStackTrace();
            }finally{
                conn.commit();
                if(con==1 && conn != null) conn.close();
            }
    }
  
    public static void delete(String tableName, String id, Connection conn) throws SQLException {
        int con=0;
        try{
            if(conn==null){
                con=1;               
                conn =  new DbPostgres().connect();
            }                        
            String sql = "DELETE FROM " + tableName + " WHERE id = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, id);
            pstmt.executeUpdate();
        }catch(Exception e){
            conn.rollback();
            e.printStackTrace();
        }finally{
            conn.commit();
            if(con==1 && conn != null) conn.close();
        }
    }
  
    public static String generateWhereClause(Object obj, String className) {
    StringBuilder sb = new StringBuilder(" ");
    Class<?> clazz;
    try {
        clazz = Class.forName("model."+className);
    } catch (ClassNotFoundException e) {
        e.printStackTrace();
        return "";
    }
    Field[] fields = clazz.getDeclaredFields();
    int count = 0;
    System.out.println("Taille field="+fields.length);
    for (Field field : fields) {
        try {
            field.setAccessible(true);
            Object value = field.get(obj);
            if (value != null) {
                if (count > 0) {
                    sb.append(" AND ");
                }
                sb.append(field.getName()).append(" like '%").append(value).append("%'");
                count++;
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    return count > 0 ? sb.toString() : "";
}
  
  
}
