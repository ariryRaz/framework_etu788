/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author tsikyrami
 */
public interface  DbInterface {
    
    // Method for establishing a connection to the database
    public Connection connect() throws SQLException;

    // Method for executing a query on the database and returning a result set
    public <T> ResultSet executeQuery(PreparedStatement statement) throws SQLException;

    // Method for executing an update (insert, update, delete) on the database and returning the number of affected rows
    public int executeUpdate(PreparedStatement statement) throws SQLException;

    // Method for closing the connection to the database
    public void close() throws SQLException;
    
    public Connection getCon() throws SQLException;
    
}
