<%-- 
    Document   : personne
    Created on : 4 f�vr. 2023, 11:58:49
    Author     : tsikyrami
--%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map.Entry"%>
<%@ page import="model.Transaction" %>
<%@ page import="model.ModelView" %>

<% ModelView render =(ModelView) request.getAttribute("data");  %>
<% List<Transaction> transaction = render.CastTo("Transaction"); %>
   
<% try { %>

 


    <div class="transaction row-justify-content-center">
        
        <div class="table_transaction">           
            
            <table id ="mytable"  >
                <form  class="bg-light border border-success" style="text-align:center;padding:25px 25px 25px 25px; border-radius:5px;" action="${pageContext.request.contextPath}/TransactionController/list.gen" method="GET">
                    <input type="hidden" name="classe" value="Transaction">
                    <input type="hidden" name="obj" value="select">

                     <tr style="text-align:center">
                        <td scope="col"> <input type="text" class="form-control" name="id"> </td>   
                        <td scope="col"> <input type="text" class="form-control" name="nom"></td>
                        <td scope="col"> <input type="text" class="form-control" name="matricule"></td>   
                        <td scope="col"> <input type="text" class="form-control" name="valeur"> </td>
                        <td scope="col"> <input type="text" class="form-control" name="type"> </td>   
                        <td class="search" colspan="3" scope=""> <button type="submit" class="btn btn-outline-success">Search</button> </td>
                     <tr>
               
                  <thead class="thead-light">                    
                    <tr style="text-align:center">
                        <th scope="col">ID</th>   
                        <th scope="col">Nom</th>
                        <th scope="col">Reference</th>   
                        <th scope="col">Quantite</th>
                        <th scope="col">Type</th>   
                        <th colspan="3" scope="">  </th>   
                    </tr>                
                  </thead>
                </form>

                <tbody>
                    <%   for(Transaction tr : transaction) { %>
                    <tr>
                        <td><%= tr.getId() %></td>
                        <td><%= tr.getNom() %></td>
                        <td><%= tr.getMatricule() %></td>
                        <td><%= tr.getValeur() %></td>
                        <td><%= tr.getType() %></td>
                        <td>
                            <form action ="${pageContext.request.contextPath}/TransactionController/transactionup.gen" method="get">
                                <input type="hidden" name="classe" value="Transaction">
                                <input type="hidden" name="obj" value="passe_id">
                                <input type="hidden" name="id" value="<%=tr.getId()%>">
                                <button type="submit" class="btn btn-success">MAJ</button>
                            </form>
                            
                        </td>
                        <td>
                            <form action ="${pageContext.request.contextPath}/TransactionController/delete.gen" method="get">
                               <input type="hidden" name="classe" value="Transaction">
                               <input type="hidden" name="obj" value="delete">
                               <input type="hidden" name="id" value="<%=tr.getId()%>">
                               <button type="submit" class="btn btn-danger">SUP</button>
                            </form>
                        </td>
                    </tr>
                    <%  } %>

                </tbody>
            </table>
                   <!--   <button  class="pdf btn btn-warning" onclick="exportToPdf()">Export to PDF</button> -->
        </div>  
    </div> 
                    
        
                    

<script>
                                  
function exportToPdf() {
  
  // S�lectionner la table � exporter
var table = document.getElementById("mytable");

table.getElementsByTagName("tr")[0].style.visibility = "hidden";

    // Cacher les deux derni�res colonnes de la table avec du CSS
    var rows = table.getElementsByTagName("tr");
    for (var i = 0; i < rows.length; i++) {
      var cells = rows[i].getElementsByTagName("td");
      if (cells.length >= 2) {
        cells[cells.length-1].style.visibility = "hidden";
        cells[cells.length-2].style.visibility = "hidden";
      }
    }

    // Exporter la table en PDF avec Html2pdf
    var opt = {
      margin: 1,
      filename: "Liste_Transaction.pdf",
      image: {type: "jpeg", quality: 0.98},
      html2canvas: {scale: 2},
      jsPDF: {unit: "in", format: "letter", orientation: "portrait"}
    };
    
        html2pdf().set(opt).from(table.outerHTML).save();
    
    
    table.getElementsByTagName("tr")[0].style.visibility = "visible";

        var rows = table.getElementsByTagName("tr");
    for (var i = 0; i < rows.length; i++) {
      var cells = rows[i].getElementsByTagName("td");
      if (cells.length >= 2) {
        cells[cells.length-1].style.visibility = "visible";
        cells[cells.length-2].style.visibility = "visible";
      }
    }


  
    }
</script>   



<%    } catch (Exception e) {
        pageContext.setAttribute("exception", e);
    }
%>

<%
    Exception ex = (Exception) request.getAttribute("exception");
    if (ex != null) {
%>
    <script type="text/javascript">
        alert("<%= ex.getMessage() %>");
    </script>
<%
    }
%>