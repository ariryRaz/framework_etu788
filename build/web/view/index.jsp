<%-- 
    Document   : index.jsp
    Created on : 13 févr. 2023, 23:00:40
    Author     : tsikyrami
--%>


<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
    <jsp:include page="head.jsp"/>
<body>
    <div class="container-fluid">
        <jsp:include page="navbar.jsp"/>
        <jsp:include page="sidebar.jsp"/>
        <div class="page">
            <jsp:include page="${pageName}"/>
        </div>

       
    </div>
</body>

</html>
