<%-- 
    Document   : PageinsertTransaction
    Created on : 25 f�vr. 2023, 14:06:43
    Author     : tsikyrami
--%>

<div class="row justify-content-md-center mt-5">
        
    <div class="col-12 col-md-6">
        
    <form  class="bg-light" style="text-align:center;padding:25px 25px 25px 25px; border-radius:5px;" action="${pageContext.request.contextPath}/TransactionController/add.gen" method="POST">

        <h3 style="text-align:center"> Ajout de nouvelle transaction: </h3>
        
        <input type="hidden" name="classe" value="Transaction">
        <input type="hidden" name="obj" value="insert">
        <input type="hidden" name="id" id="myTextInput">
         
        <div  class="form-group row mt-5">
            <label for="input" class="col-sm-2 col-form-label">Nom</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="nom" placeholder="Nom">
            </div>
        </div>
        <div class="form-group row">
            <label for="input" class="col-sm-2 col-form-label">Reference</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="matricule" placeholder="Reference">
            </div>
        </div>
        <div class="form-group row">
            <label for="input" class="col-sm-2 col-form-label">Quantite</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" name="valeur" placeholder="Quantite">
            </div>
        </div>
         <div class="form-group row">
         <label for="input" class="col-sm-2 col-form-label">Type</label>
            <div class="col-sm-10">
                <select class="custom-select" name="type">
                    <option selected>Choisir</option>
                    <option value="Achat">Achat</option>
                    <option value="Vente">Vente</option>
                </select>
            </div>
         </div>
         
        <button type="submit" class="btn btn-success mt-3"> Inserer </button>
    </form>
</div>
    
</div>

<script>

  var textInput = document.getElementById('myTextInput');
  var timestamp = new Date().getTime(); 
  var val = BigInt(timestamp); 
  textInput.value = "TR_"+val;
</script>

