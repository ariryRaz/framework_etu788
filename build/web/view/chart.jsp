<%@page import="java.util.List"%>
<%@page import="java.util.Map.Entry"%>
<%@ page import="model.Transaction" %>
<%@ page import="model.ModelView" %>

<% ModelView render =(ModelView) request.getAttribute("data");  %>
<% List<Transaction> transaction = render.CastTo("Transaction"); %>

    <h1> Transaction</h1>
    <canvas id="chart"></canvas>

	<script>
		var data = {
			labels: [
                            <% for(Transaction tr :transaction) { %>
                                    "<%= tr.getNom() %>",
                            <% } %>
			],
			datasets: [{
                            label: "List Of Transaction",
                            data: [
                            <%  for(Transaction tr :transaction) { %>
                                    "<%= tr.getValeur() %>",
                            <% } %>
                            ],
                            backgroundColor: [
                                'rgba(255, 99, 132,0.6)',
                                'rgba(75, 192, 192,0.6)',
                                'rgba(255, 205, 86,0.6)',
                                'rgba(201, 203, 207,0.6)',
                                'rgba(54, 162, 235,0.6)'
                            ]
			}]
		};
		var chart = new Chart(document.getElementById("chart"), {
			type: 'pie',
			data: data
		});
	</script>