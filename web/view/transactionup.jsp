
<%@page import="java.util.List"%>
<%@page import="java.util.Map.Entry"%>
<%@ page import="model.Transaction" %>
<%@ page import="model.ModelView" %>

<% ModelView render =(ModelView) request.getAttribute("data");  %>
<% List<Transaction> transaction = render.CastTo("Transaction"); %>
<% Transaction tr = transaction.get(0); %>

    <div class="row justify-content-md-center mt-5">

        <div class="col-12 col-md-6">

        <form  class="bg-light" style="text-align:center;padding:25px 25px 25px 25px; border-radius:5px;" action="${pageContext.request.contextPath}/TransactionController/update.gen" method="POST">

            <h3 style="text-align:center;margin-bottom: 30px"> Mettre a jour </h3>

            <input type="hidden" name="classe" value="Transaction">
            <input type="hidden" name="obj" value="update">
            
            
            <div  class="form-group row">
                <label for="input" class="col-sm-2 col-form-label">Id</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="id" value="<%=tr.getId()%>" readonly>
                </div>
            </div>
            <div  class="form-group row">
                <label for="input" class="col-sm-2 col-form-label">Nom</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="nom" value="<%=tr.getNom()%>">
                </div>
            </div>
            <div class="form-group row">
                <label for="input" class="col-sm-2 col-form-label">Reference</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="matricule" value="<%=tr.getMatricule()%>">
                </div>
            </div>
            <div class="form-group row">
                <label for="input" class="col-sm-2 col-form-label">Quantite</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="valeur" value="<%=tr.getValeur()%>">
                </div>
            </div>

                 
           
            <div class="form-group row">
                <label for="input" class="col-sm-2 col-form-label">Type</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="type" value="<%=tr.getType()%>">
                </div>
            </div>
                   
                <button type="submit" class="btn btn-success"> Mettre a jour </button>
          
        </form>
    </div>

    </div>


                    
