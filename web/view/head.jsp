<%-- 
    Document   : head
    Created on : 14 f�vr. 2023, 08:58:56
    Author     : tsikyrami
--%>

<head>
    <title>Page JSP</title>
            <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/scss/navbar.scss"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/scss/sidebar.scss"/>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/scss/page.scss"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/scss/transaction.scss"/>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/scss/footer.scss"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css"/>
    <script src="${pageContext.request.contextPath}/js/Chart.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/html2pdf.js"></script>
 
</head>